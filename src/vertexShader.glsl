#version 460 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;

out vec3 finalColor;

void main()
{
  // read the multiplication from right to left
  // gl_Position = vec4(aPos, 1.0f);
  gl_Position = projectionMat *viewMat * modelMat * vec4(aPos.x, aPos.y, aPos.z, 1.0);
  finalColor = aColor;
};
