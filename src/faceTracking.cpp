#include "LandmarkCoreIncludes.h"
#include "GazeEstimation.h"

#include <SequenceCapture.h>
#include <Visualizer.h>
#include <VisualizationUtils.h>

#include <iostream>
#include "faceTracking.h"
#define INFO_STREAM(stream) \
    std::cout << stream << std::endl

#define WARN_STREAM(stream) \
    std::cout << "Warning: " << stream << std::endl

#define ERROR_STREAM(stream) \
    std::cout << "Error: " << stream << std::endl

static void printErrorAndAbort(const std::string &error)
{
    std::cout << error << std::endl;
    abort();
}

#define FATAL_STREAM(stream) \
    printErrorAndAbort(std::string("Fatal error: ") + stream)

void createFaceTracker()
{
    // these values can be changed to adjust the settings of the tracker
    // fx, fy can also be added to the settings file, but are not currently used
    std::vector<std::string> arguments = {"-device", "0"};

    LandmarkDetector::FaceModelParameters det_parameters(arguments);

    // The modules that are being used for tracking
    LandmarkDetector::CLNF face_model(det_parameters.model_location);
    if (!face_model.loaded_successfully)
    {
        std::cout << "ERROR: Could not load the landmark detector" << std::endl;
    }

    if (!face_model.eye_model)
    {
        std::cout << "WARNING: no eye model found" << std::endl;
    }

    // Open a sequence
    Utilities::SequenceCapture sequence_reader;

    // // // A utility for visualizing the results (show just the tracks)
    // // Utilities::Visualizer visualizer(true, false, false, false);

    // // Tracking FPS for visualization
    // // Utilities::FpsTracker fps_tracker;
    // // fps_tracker.AddFrame();

    int sequence_number = 0;

    while (true)
    {

        // The sequence reader chooses what to open based on command line arguments provided
        if (!sequence_reader.Open(arguments))
            break;

        INFO_STREAM("Device or file opened");

        cv::Mat rgb_image = sequence_reader.GetNextFrame();

        INFO_STREAM("Starting tracking");
        while (!rgb_image.empty())
        {

            // Reading the images
            cv::Mat_<uchar> grayscale_image = sequence_reader.GetGrayFrame();

            // The actual facial landmark detection / tracking
            bool detection_success = LandmarkDetector::DetectLandmarksInVideo(rgb_image, face_model, det_parameters, grayscale_image);

            //         // Gaze tracking, absolute gaze direction
            //         cv::Point3f gazeDirection0(0, 0, -1);
            //         cv::Point3f gazeDirection1(0, 0, -1);

            //         // If tracking succeeded and we have an eye model, estimate gaze
            //         if (detection_success && face_model.eye_model)
            //         {
            //         	GazeAnalysis::EstimateGaze(face_model, gazeDirection0, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy, true);
            //         	GazeAnalysis::EstimateGaze(face_model, gazeDirection1, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy, false);
            //         }

            // Work out the pose of the head from the tracked model
            cv::Vec6d pose_estimate = LandmarkDetector::GetPose(face_model, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy);
            for (int i = 0; i < 6; i++)
            {
                std::cout << pose_estimate[i] << " ";
            }
            std::cout << std::endl;
            //         // Keeping track of FPS
            //         // fps_tracker.AddFrame();

            //         // Displaying the tracking visualizations
            //         // visualizer.SetImage(rgb_image, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy);
            //         // visualizer.SetObservationLandmarks(face_model.detected_landmarks, face_model.detection_certainty, face_model.GetVisibilities());
            //         // visualizer.SetObservationPose(pose_estimate, face_model.detection_certainty);
            //         // visualizer.SetObservationGaze(gazeDirection0, gazeDirection1, LandmarkDetector::CalculateAllEyeLandmarks(face_model), LandmarkDetector::Calculate3DEyeLandmarks(face_model, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy), face_model.detection_certainty);
            //         // visualizer.SetFps(fps_tracker.GetFPS());
            //         // detect key presses (due to pecularities of OpenCV, you can get it when displaying images)
            //         // char character_press = visualizer.ShowObservation();

            //         // restart the tracker
            //         // if (character_press == 'r')
            //         // {
            //         // 	face_model.Reset();
            //         // }
            //         // // quit the application
            //         // else if (character_press == 'q')
            //         // {
            //         // 	return(0);
            //         // }

            // Grabbing the next frame in the sequence
            rgb_image = sequence_reader.GetNextFrame();
        }

        // Reset the model, for the next video
        face_model.Reset();
        sequence_reader.Close();

        sequence_number++;
    }
}
