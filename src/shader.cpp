#include "shader.h"
#include <string>
#include <fstream>
#include <sstream>

Shader::Shader(const char *vertexShaderPath, const char *fragmentShaderPath)
{
    // Get shader source code from respective file paths
    std::string vertexCode;
    std::string fragmentCode;

    std::ifstream vShaderFile;
    std::ifstream fShaderFile;
    // ensure ifstream objects can throw exceptions:
    vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try
    {
        vShaderFile.open(vertexShaderPath);
        fShaderFile.open(fragmentShaderPath);
        std::stringstream vShaderStream, fShaderStream;

        // read file's buffer contents into streams
        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf();

        // close file handlers
        vShaderFile.close();
        fShaderFile.close();

        // convert stream into string
        vertexCode = vShaderStream.str();
        fragmentCode = fShaderStream.str();
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }

    const char *vertexShaderCode = vertexCode.c_str();
    const char *fragmentShaderCode = fragmentCode.c_str();

    /* Creating the vertex shader */
    unsigned int vertexShader;
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderCode, nullptr);
    glCompileShader(vertexShader);

    /* Checking for compile time errors for the vertex shader */
    int VSsuccess;
    char VSinfoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &VSsuccess);

    if (!VSsuccess)
    {
        glGetShaderInfoLog(vertexShader, 512, nullptr, VSinfoLog);
        std::cout << "Error: Vertex shader compilation failed\n"
                  << VSinfoLog << std::endl;
    }
    else
    {
        std::cout << "Vertex shader compiled successfully\n";
    }

    /* Creating the fragment shader */
    unsigned int fragmentShader;
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderCode, nullptr);
    glCompileShader(fragmentShader);

    /* Checking for compile time errors for the vertex shader */
    int FSsuccess;
    char FSinfoLog[512];
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &FSsuccess);

    if (!FSsuccess)
    {
        glGetShaderInfoLog(fragmentShader, 512, nullptr, FSinfoLog);
        std::cout << "Error: Fragment shader compilation failed\n"
                  << FSinfoLog << std::endl;
    }
    else
    {
        std::cout << "Fragment shader compiled successfully\n";
    }

    /* Creating a shader program */
    m_ShaderProgramID = glCreateProgram();

    /* Attaching the shaders and linking them */
    glAttachShader(m_ShaderProgramID, vertexShader);
    glAttachShader(m_ShaderProgramID, fragmentShader);
    glLinkProgram(m_ShaderProgramID);

    /* Check for shader program linking */
    int SPsuccess;
    char SPinfoLog[512];
    glGetProgramiv(m_ShaderProgramID, GL_LINK_STATUS, &SPsuccess);

    if (!SPsuccess)
    {
        glGetProgramInfoLog(m_ShaderProgramID, 512, nullptr, SPinfoLog);
        std::cout << "Error: Shader program linking failed\n"
                  << SPinfoLog << std::endl;
    }
    else
    {
        std::cout << "Shader program linked successfully\n";
    }

    // After linking, use the shader program
    // Every shader and rendering call after this will now use this program object (and thus the shaders).
    glUseProgram(m_ShaderProgramID);

    // Delete the shader objects once we've linked them to the program object
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

void Shader::useShader()
{
    glUseProgram(m_ShaderProgramID);
}

void Shader::setBool(const std::string &name, bool value) const
{
    glUniform1i(glGetUniformLocation(m_ShaderProgramID, name.c_str()), (int)value);
}

void Shader::setInt(const std::string &name, int value) const
{
    glUniform1i(glGetUniformLocation(m_ShaderProgramID, name.c_str()), (int)value);
}

void Shader::setFloat(const std::string &name, float value) const
{
    glUniform1i(glGetUniformLocation(m_ShaderProgramID, name.c_str()), (float)value);
}
