#include <iostream>
#include <glad/gl.h>
#include <GLFW/glfw3.h>
#include <math.h>

#include "utils.h"
#include "faceTracking.h"
#include "shader.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "LandmarkCoreIncludes.h"
#include "GazeEstimation.h"

#include <SequenceCapture.h>
#include <Visualizer.h>
#include <VisualizationUtils.h>

/*
    The 3D normalised(between -1.0 to 1.0) vertices of the triangle
    These normalised coordinates are the final vertices that go into OpenGL origin: center;
    +X towards right and +Y towards up-direction
*/

// vertices for a triangle
// float vertices[] = {
// positions       // colors
// 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f,  // bottom right
// -0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, // bottom left
// 0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f    // top
double distanceScalingFactor = 0.0f;

// vertices along with the indices for a cube
float vertices[] = {
    0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.5f,
    -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 0.5f,
    0.5f, 0.5f, -0.5f, 1.0f, 1.0f, 0.5f,
    0.5f, 0.5f, -0.5f, 1.0f, 1.0f, 0.5f,
    -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.5f,
    -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 0.5f,
    -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 0.5f,

    0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.5f,
    0.5f, 0.5f, 0.5f, 1.0f, 1.0f, 0.5f,
    0.5f, 0.5f, 0.5f, 1.0f, 1.0f, 0.5f,
    -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.5f,
    -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 0.5f,

    -0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.5f,
    -0.5f, 0.5f, -0.5f, 1.0f, 1.0f, 0.5f,
    -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,
    -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,
    -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 0.5f,
    -0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.5f,

    0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.5f,
    0.5f, 0.5f, -0.5f, 1.0f, 1.0f, 0.5f,
    0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,
    0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,
    0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 0.5f,
    0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.5f,

    -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,
    0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 0.5f,
    0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.5f,
    0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.5f,
    -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 0.5f,
    -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,

    -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.5f,
    0.5f, 0.5f, -0.5f, 1.0f, 1.0f, 0.5f,
    0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.5f,
    0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.5f,
    -0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 0.5f,
    -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.5f};

glm::vec3 getPositionChange(glm::vec3 finalPosition, glm::vec3 initialPosition)
{
    if (initialPosition == glm::vec3(0.0f, 0.0f, 0.0f))
    {
        return glm::vec3(0.0f, 0.0f, 0.0f);
    }
    glm::vec3 positionChange = finalPosition - initialPosition;
    std::cout << distanceScalingFactor << std::endl;
    positionChange *= distanceScalingFactor;
    return positionChange;
}

int main(void)
{
    // these values can be changed to adjust the settings of the tracker
    // fx, fy can also be added to the settings file, but are not currently used
    std::vector<std::string> arguments = {"-device", "0"};

    LandmarkDetector::FaceModelParameters det_parameters(arguments);

    // The modules that are being used for tracking
    LandmarkDetector::CLNF face_model(det_parameters.model_location);
    if (!face_model.loaded_successfully)
    {
        std::cout << "ERROR: Could not load the landmark detector" << std::endl;
    }

    if (!face_model.eye_model)
    {
        std::cout << "WARNING: no eye model found" << std::endl;
    }

    // Open a sequence
    Utilities::SequenceCapture sequence_reader;

    // createFaceTracker();
    GLFWwindow *window;

    /* Initialize the glfw library */
    if (!glfwInit())
        return -1;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(1920, 1080, "catfishtank", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    int version = gladLoadGL(glfwGetProcAddress);
    if (version == 0)
    {
        printf("Failed to initialize OpenGL context\n");
        return -1;
    }

    // Successfully loaded OpenGL
    printf("Loaded OpenGL %d.%d\n", GLAD_VERSION_MAJOR(version), GLAD_VERSION_MINOR(version));

    int nrAttributes;
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
    std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;

    // Creating the shader using shader class
    const char *vertexShaderPath = "../src/vertexShader.glsl";
    const char *fragmentShaderPath = "../src/fragmentShader.glsl";
    Shader shaderProgram(vertexShaderPath, fragmentShaderPath);

    // Activate the shader
    shaderProgram.useShader();

    unsigned int VAO = setupVertexBuffer(vertices, sizeof(vertices));

    // The loading of the OpenFace can be done here
    // Until then...
    delay(3);

    // The sequence reader chooses what to open based on command line arguments provided
    if (!sequence_reader.Open(arguments))
    {
        std::cout << "ERROR: Could not open the sequence" << std::endl;
        return -1;
    }
    else
    {
        std::cout << "Device or file opened\n";
    }

    // the current image captured by the webcam
    cv::Mat rgb_image = sequence_reader.GetNextFrame();

    std::cout << "Starting tracking\n";

    cv::Vec6d pose_estimate = {};
    glm::vec3 currentPoseEstimate = glm::vec3(0.0f);
    glm::vec3 previousPoseEstimate = glm::vec3(0.0f);

    /* Create transformation matrices for different coordinate system transformations */

    // view matrix
    glm::mat4 viewMat = glm::mat4(1.0f);
    viewMat = glm::translate(viewMat, glm::vec3(0.0f, 0.0f, -3.0f)); // initial position of the cube

    // model matrix
    glm::mat4 modelMat = glm::mat4(1.0f);
    modelMat = glm::rotate(modelMat, glm::radians(-55.0f), glm::vec3(1.0f, 1.0f, 1.0f));

    // projection matrix
    glm::mat4 projectionMat;
    // creates a perspective projection matrix
    projectionMat = glm::perspective(
        glm::radians(45.0f), // fov value; to set how large the viewspace is; generally 45 degrees
        1920.0f / 1080.0f,   // aspect ratio; height / width
        0.1f,                // near plane
        100.0f               // far plane
    );

    // Set the value of the uniforms (This is done per frame; makes sense)
    int modelLoc = glGetUniformLocation(shaderProgram.m_ShaderProgramID, "modelMat");
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelMat));

    int viewLoc = glGetUniformLocation(shaderProgram.m_ShaderProgramID, "viewMat");
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(viewMat));

    int projectionLoc = glGetUniformLocation(shaderProgram.m_ShaderProgramID, "projectionMat");
    glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projectionMat));

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        if (!rgb_image.empty())
        {
            // Reading the images
            cv::Mat_<uchar> grayscale_image = sequence_reader.GetGrayFrame();

            // The actual facial landmark detection / tracking
            bool detection_success = LandmarkDetector::DetectLandmarksInVideo(rgb_image, face_model, det_parameters, grayscale_image);

            // Work out the pose of the head from the tracked model
            pose_estimate = LandmarkDetector::GetPose(face_model, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy);

            // pose_estimate = LandmarkDetector::GetPoseWRTCamera(face_model, sequence_reader.fx, sequence_reader.fy, sequence_reader.cx, sequence_reader.cy);
            for (int i = 0; i < 6; i++)
            {
                std::cout << pose_estimate[i] << " ";
            }
            std::cout << std::endl;

            // Grabbing the next frame in the sequence
            rgb_image = sequence_reader.GetNextFrame();
        }
        currentPoseEstimate = glm::vec3(pose_estimate[0], pose_estimate[1], pose_estimate[2]);
        if (distanceScalingFactor == 0)
        {
            // Scale the distance between the eyes
            distanceScalingFactor = 3.0f / pose_estimate[2];
        }
        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT);         // clears the previous frame's color buffer
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f); // sets the background color, by setting the color buffer

        // translating the scene in the reverse direction where we want the camera to move
        // we want the camera to move in +z hence we need to move the scene in -z
        glm::vec3 change = getPositionChange(currentPoseEstimate, previousPoseEstimate);
        viewMat = glm::translate(viewMat, glm::vec3(change.x, change.y, -change.z));

        int viewLoc = glGetUniformLocation(shaderProgram.m_ShaderProgramID, "viewMat");
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(viewMat));

        // Now rendering the triangles
        // glBindVertexArray(VAO);
        // glDrawArrays(
        //     GL_TRIANGLES, // the primitive type we need to draw
        //     0,            // starting index of the vertex array
        //     3             // the number of vertices we need to draw
        // );
        glDrawArrays(GL_TRIANGLES, 0, 36);

        // Tell OpenGl to enable depth testing
        glEnable(GL_DEPTH_TEST);
        // clears the previous value of the depth buffer so that next frame calculations aren't affected by previous values
        glClear(GL_DEPTH_BUFFER_BIT);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();

        // update the previous pose estimate
        previousPoseEstimate = currentPoseEstimate;
    }

    // Reset the model, for the next video
    face_model.Reset();
    sequence_reader.Close();

    glfwTerminate();
    return 0;
}
