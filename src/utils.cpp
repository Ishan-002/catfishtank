#include "utils.h"
#include <iostream>
#include <glad/gl.h>
// for delay related calls
#include <chrono>
#include <thread>

void delay(int seconds)
{
    std::cout << "Hello waiter" << std::endl;
    std::chrono::seconds dura(seconds);
    std::this_thread::sleep_for(dura);
    std::cout << "Waited " << seconds << " seconds" << std::endl;
}

unsigned int setupVertexBuffer(float vertices[], size_t verticesSize)
{
    // Create a VAO
    unsigned int VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    /* create a Vertex buffer object */
    unsigned int VBO;
    // generate the vertex buffer object to store the vertices
    glGenBuffers(1, &VBO);
    /* binds the buffer object to the target buffer type.
    From that point on any buffer calls we make (on the GL_ARRAY_BUFFER target) will be used to configure the currently bound buffer, which is VBO */
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // copy the previously defined vertex data into the buffer's memory
    glBufferData(GL_ARRAY_BUFFER, verticesSize, vertices, GL_STATIC_DRAW);

    /*
        Never use sizeof(arrayName) when array is passed as a parameter
        This is because array parameters are treated as pointers, and sizeof() would compute the size of the pointer.
        Refer: https://www.geeksforgeeks.org/using-sizof-operator-with-array-paratmeters-in-c
    */

    // Linking vertex attributes i.e telling the shader how data is stored in the buffer
    // This means to manually specify what part of our input data goes to which vertex attribute in the vertex shader
    glVertexAttribPointer(
        0,                 // the vertex attribute location we want to configure; mentioned in the pixel shader
        3,                 // size of vertex attribute. 3 values for our case (vec3)
        GL_FLOAT,          // type of the data
        GL_FALSE,          // to normalise the data or not. (we already have our data normalised)
        6 * sizeof(float), // stride; the space between consecutive vertex attributes. We could also have set the stride to 0, since we have no gap between our consecutive attributes, then OpenGL would determine stride itself later
        (void *)0          // offset; the position where the data for the attribute begins (needs cast for some reason)
    );
    glEnableVertexAttribArray(0); // enable the vertex attribute (disabled by default)

    // Linking the color attributes in a similar way
    glVertexAttribPointer(
        1, // the second vertex attribute location .i.e for colors
        3,
        GL_FLOAT,
        GL_FALSE,
        6 * sizeof(float),            // same vertex hence same size
        (void *)(3 * sizeof(float))); // this time the offset is 3*sizeof(float); the initial 3 float values being occupied by vertex values
    glEnableVertexAttribArray(1);
    return VAO;
}
