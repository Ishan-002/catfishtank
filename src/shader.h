#pragma once
#include <glad/gl.h>
#include <iostream>

class Shader
{
private:
    Shader() = delete;
    Shader(const Shader &) = delete;

public:
    unsigned int m_ShaderProgramID;

    Shader(const char *vertexShaderPath, const char *fragmentShaderPath);
    void useShader();

    void setBool(const std::string &name, bool value) const;
    void setInt(const std::string &name, int value) const;
    void setFloat(const std::string &name, float value) const;
};
