# catfishtank

### Setup instructions
1. Create `build/` directory at the project root.
2. Build using CMake:
```
cd build/
cmake ..
make
```
3. Run the executable located in `build` using `./catfishtank`.
